

import 'package:flutter/material.dart';
import 'package:flutter_films_students_app/model/movie.dart';

import 'addMovie.dart';

class MovieListView extends StatefulWidget{
  @override
  _MovieListViewState createState()=> _MovieListViewState();
}

class _MovieListViewState extends State<MovieListView> {
  List<Movie> movieList = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Mes Films"),
          backgroundColor: Colors.blueGrey.shade900,
          actions: [
            IconButton(
                icon: Icon(Icons.file_download, color: Colors.white),
                onPressed: () {
                  _downloadList();
                }
            ),
            IconButton(
              icon: Icon(Icons.add, color: Colors.white,),
              onPressed: () {
                _addMovie(context);
              },
            )
          ],
        ),
        backgroundColor: Colors.blueGrey.shade400,
        body: ListView.builder(
          itemCount: movieList.length,
          itemBuilder: (BuildContext context, int index) {
            //fonction permettant de construire le widget a la position index de la listview
            return Card(
                elevation: 5, //ombre
                color: Colors.white,
                child: ListTile(
                  leading: CircleAvatar(
                    //placer un widget a gauche
                    child: Container(
                      width: 200,
                      height: 200,
                      decoration: BoxDecoration(
                        //color: Colors.blue,
                        image: DecorationImage(
                            image: NetworkImage(movieList[index].images[0]),
                            fit: BoxFit.cover
                        ),
                        borderRadius: BorderRadius.circular(15),
                      ),
                      child: Text("F"),
                    ),
                  ),
                  title: Text(movieList[index].title),
                  subtitle: Text(movieList[index].genre),
                  trailing: Text("..."),
                  onTap: () {
                    debugPrint("Movie : ${movieList
                        .elementAt(index)
                        .title}");
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                MovieDetails(movie:
                                movieList.elementAt(index)))
                    );
                  },
                )
            );
          },)
    );
  }

  void _downloadList() {
    Movie.loadMovies().then((m) {
      setState(() {
        if (m != null) {
          m.forEach((item) => movieList.add(item));
        } else {
          movieList = [];
        }
      });
    });
  }

  void _addMovie(BuildContext context) async {
    final newMovie = await Navigator.push(context,
        MaterialPageRoute(builder: (context) => AddMovie()));
    if (newMovie != null) {
      setState(() {
        movieList.add(newMovie);
      });
    } else {
      debugPrint("pas de nouveau film");
    }
  }
}

class MovieDetails extends StatelessWidget{
  final Movie movie;

  const MovieDetails({Key key, this.movie}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("${this.movie.title} details")),
      body: Container(
        child:Center(
          child:RaisedButton(
            child:Text("GoBack"),
            onPressed: (){
              Navigator.pop(context);
            },
          )
        )
      )
    );
  }

}